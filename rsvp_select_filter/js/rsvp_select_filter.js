(function ($, Drupal) {
  Drupal.behaviors.rsvpSelectFilter = {
    attach: function (context, settings) {
      $('#edit-field-primary-community', context).once('community-select').change(function () {
        $(this).closest('form').submit();
      });
    }
  };
})(jQuery, Drupal);
