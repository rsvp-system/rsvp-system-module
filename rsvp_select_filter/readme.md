#### - Use AJAX in hook_form_alter() to trigger updates on the child (resource) select list.

#### - Use a Views-based entity reference field to dynamically repopulate available options.

#### - Leverage Views filtering to ensure only valid selections are displayed.

A helper to call hook form alter, sets up a dynamic Parent (Community) / Child (Resource) filter relationship. Used with Views to filter the select lists on on the content creation form. 

When adding a new request, the available Resources are trimmed to their Community (taxonomy). Changing the Parent Community select value will adjust the Child Resource select list. 

A "Primary Community" value from the current user's account, along with Role memberships, are used to trim the available items in both select lists.  

The RSVP Request Form is trimmed based on the current user's Role memberships and their Primary Community. Accounts have a Primary Community refernece field that is used for setting content paths, views filtering and content access. 